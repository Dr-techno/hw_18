<?php
namespace Core;

class Model
{
   public $db;

   public function __construct(){

    	try{
			$pdo = new \PDO('mysql:host=localhost;dbname='.DB_NAME,DB_USER,DB_PASS);
			$pdo->setAttribute(\PDO::ATTR_ERRMODE,\PDO::ERRMODE_EXCEPTION);
			$pdo->exec("SET NAMES 'utf8'");
			
			$this->db = $pdo;

		}catch(\PDOException $e){
			echo "Не получилось подключиться к Базе Данных.<br>";
			echo $e->getMessage();
			exit();
		}
    }

}