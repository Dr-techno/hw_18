<?php
namespace Core;

class Controller
{
    public $model;
    public $view;

    public function __construct()
    {
        $this->view = new View();
    }

    public function checkInt($int){

        if(is_numeric($int)){
            return $int;
        }else{
            route::ErrorPage404();
        }

    }
    public function action_index()
    {

    }

}
